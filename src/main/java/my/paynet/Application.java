package my.paynet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@RestController
	public static class Api {

		private final JdbcTemplate jdbcTemplate;

		@Autowired
		public Api(JdbcTemplate jdbcTemplate) {
			this.jdbcTemplate = jdbcTemplate;
		}

		@GetMapping
		public String time() {
			final String timestamp = jdbcTemplate.queryForObject("select service_type_id from rpp_service_type where SERVICE_TYPE_CODE ='030'", String.class);
			
			return "Service type for QR  " + timestamp;
		}
	}
}
