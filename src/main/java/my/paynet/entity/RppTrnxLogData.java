package my.paynet.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RPP_TRNX_LOG_DATA")
public class RppTrnxLogData {

	@Id
	@Column(name = "ROWID")
	private String rowid;

	@Column(name = "BUSINESS_MSG_ID")
	private String businessmsgid;

	@Column(name = "SERVICE")
	private String service;

	@Column(name = "OFI_ACCT_NAME")
	private String ofiaccountname;

	@Column(name = "OFI_CODE")
	private String ofiCode;

	@Column(name = "REGISTRATION_ID")
	private String registrationId;

	@Column(name = "INITIATION_DATE")
	private Date initiationdate;

	/*
	 * @Column(name="TRNX_AMOUNT") private long trnxamount;
	 */

	@Column(name = "TRNX_AMOUNT")
	private String trnxamount;

	@Column(name = "RECEIPT_REF")
	private String recpref;

	@Column(name = "PAYMENT_DESC")
	private String paymentdesc;

	/*
	 * @Column(name="TRNX_STATUS") private String trnxstatus;
	 * 
	 * @Column(name="TRNX_SUB_CODE") private String trnxsubcode;
	 */

	/*
	 * public long getTrnxamount() { return trnxamount; }
	 * 
	 * public void setTrnxamount(long trnxamount) { this.trnxamount = trnxamount; }
	 */

	public String getRowid() {
		return rowid;
	}

	public String getTrnxamount() {
		return trnxamount;
	}

	public void setTrnxamount(String trnxamount) {
		this.trnxamount = trnxamount;
	}

	public void setRowid(String rowid) {
		this.rowid = rowid;
	}

	public String getBusinessmsgid() {
		return businessmsgid;
	}

	public void setBusinessmsgid(String businessmsgid) {
		this.businessmsgid = businessmsgid;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getOfiaccountname() {
		return ofiaccountname;
	}

	public void setOfiaccountname(String ofiaccountname) {
		this.ofiaccountname = ofiaccountname;
	}

	public Date getInitiationdate() {
		return initiationdate;
	}

	public void setInitiationdate(Date initiationdate) {
		this.initiationdate = initiationdate;
	}

	/*
	 * public String getTrnxstatus() { return trnxstatus; }
	 * 
	 * public void setTrnxstatus(String trnxstatus) { this.trnxstatus = trnxstatus;
	 * }
	 */

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getOfiCode() {
		return ofiCode;
	}

	public void setOfiCode(String ofiCode) {
		this.ofiCode = ofiCode;
	}

	public String getRecpref() {
		return recpref;
	}

	public void setRecpref(String recpref) {
		this.recpref = recpref;
	}

	public String getPaymentdesc() {
		return paymentdesc;
	}

	public void setPaymentdesc(String paymentdesc) {
		this.paymentdesc = paymentdesc;
	}

}
