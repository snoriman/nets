package my.paynet.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RPP_LAST_MODIF_DATE")
public class RppLastModifDate {

	/*
	 * @Id
	 * 
	 * @Column(name="ROWID")
	 * 
	 * @GeneratedValue(strategy= GenerationType.SEQUENCE) private String rowid;
	 */

	@Id
	@Column(name = "CODE")
	private String code;

	@Column(name = "LAST_MODIF_DATE")
	private Date lastmodifidate;

	@Column(name = "IS_PROCESSING")
	private int isprocessing;

	@Column(name = "MK_PENDING")
	private String mkpending;

	/*
	 * public String getRowid() { return rowid; }
	 * 
	 * public void setRowid(String rowid) { this.rowid = rowid; }
	 */
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getLastmodifidate() {
		return lastmodifidate;
	}

	public void setLastmodifidate(Date lastmodifidate) {
		this.lastmodifidate = lastmodifidate;
	}

	public int getIsprocessing() {
		return isprocessing;
	}

	public void setIsprocessing(int isprocessing) {
		this.isprocessing = isprocessing;
	}

	public String getMkpending() {
		return mkpending;
	}

	public void setMkpending(String mkpending) {
		this.mkpending = mkpending;
	}

}
