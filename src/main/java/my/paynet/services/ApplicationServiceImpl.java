package my.paynet.services;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import my.paynet.entity.RppTrnxLogData;
import my.paynet.repository.RppTrnxLogDataRepository;


@Service("applicationService")
//@Transactional
public class ApplicationServiceImpl implements ApplicationService {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationServiceImpl.class);

	@Autowired
	RppTrnxLogDataRepository trnxRepo;

	@Override
	public void getTranxData() {
		// TODO Auto-generated method stub
		List<RppTrnxLogData> txns= trnxRepo.getTxnReportDataFilterByVoterMsisdn();
		for(RppTrnxLogData data : txns) {
		logger.info("{bizid:"+data.getBusinessmsgid()+" ,  ofi: "+data.getOfiCode()+"}");
		}
		
	}

	@Override
	public void updateTrxnData(String Data) {
		// TODO Auto-generated method stub
		 
	}


}
