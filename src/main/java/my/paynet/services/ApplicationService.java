package my.paynet.services;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;


public interface  ApplicationService {
	public void getTranxData();
	public void updateTrxnData(String Data);

}
