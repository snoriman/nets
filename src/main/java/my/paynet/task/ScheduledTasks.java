package my.paynet.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import my.paynet.services.ApplicationService;




@Configuration
@EnableScheduling
public class ScheduledTasks {

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
	
	@Autowired
	ApplicationService service;
	
	@Scheduled(cron = "*/30 * * * * *")
	public void jobTask01() throws Exception {
		logger.info("Start jobTask01");
		service.getTranxData();
		logger.info("End jobTask01");
	
	}
	
	
	

	
	
}
