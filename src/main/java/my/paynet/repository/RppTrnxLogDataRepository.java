package my.paynet.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import my.paynet.entity.RppTrnxLogData;


@Repository
public interface RppTrnxLogDataRepository extends CrudRepository<RppTrnxLogData, Long> {

	@Query(nativeQuery = true, value = "SELECT ROWID,BUSINESS_MSG_ID,SERVICE,OFI_ACCT_NAME,REGISTRATION_ID,INITIATION_DATE,TRNX_AMOUNT,OFI_CODE,RECEIPT_REF,PAYMENT_DESC "
			+ "FROM RPP_TRNX_LOG_DATA WHERE TRNX_STATUS='ACSP' AND  TRNX_SUB_CODE='U000' AND ROWNUM<100")
	List<RppTrnxLogData> getTxnReportDataFilterByVoterMsisdn();
	
	

}
