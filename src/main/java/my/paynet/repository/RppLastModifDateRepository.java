package my.paynet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import my.paynet.entity.RppLastModifDate;


@Repository
public interface RppLastModifDateRepository extends CrudRepository<RppLastModifDate, Long> {

	public RppLastModifDate findByCode(String code);
	
	
	
}
